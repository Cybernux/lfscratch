# LFScratch

Build a [Linux From Scratch](http://www.linuxfromscratch.org) Distro with this set of scripts.

The base of these scripts are from [LFScript](https://www.lfscript.org). Why am I using this? Well I do like the idea of hoe these scripts work together and the simplicity (when they work). I don't like the idea of using SVN for building a distro. I want to use the stable LFS Books.

This means, at least for now, manully preparing for the latest stable (9.1 at this point) LFS release. I have not had the ***"factory"** work at all for me, and I've been trying sine 2010. This may be a reason to remove it.


## License
Same as the original. MIT, see [LICENSE](LICENSE) for details.

## Documentation
Until documention is available here on the [WIKI](../../wiki/home), you will find the available docs at [LFScript](https://www.lfscript.org) useful.

As documentaion becomes available, notification will be made here.

